create table person (
                        id serial primary key,
                        fio varchar(255),
                        phone int
);
select * from  person;
insert into person (fio, phone) values
('Лукин А.В.', '8908455'),
('Кузнецов В.Ю.', '845521'),
('Иванов С.А.', '8772356'),
('Алексеев А.П.', '89085235');
update person set phone = 111111 where id = 2;
delete from person where id = 3;